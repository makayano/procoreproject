﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProcoreProject
{
    public class NumAPI
    {
        public string text { get; set; }
        public int number { get; set; }
        public bool found { get; set; }
        public string type { get; set; }

        public static async Task<NumAPI> GetRequest(int input)
        {
            HttpClient client = new HttpClient();
            Uri getUri = new Uri("http://numbersapi.com/" + input + "?json");
            HttpResponseMessage response = await client.GetAsync(getUri);
            string randomFact = "";
            NumAPI numberInfo = new NumAPI();
            if (response.IsSuccessStatusCode)
            {
                randomFact = await response.Content.ReadAsStringAsync();
                numberInfo = JsonConvert.DeserializeObject<NumAPI>(randomFact);
            }
            return numberInfo;
        }
    }
}
