﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Windows.UI.Popups;

namespace ProcoreProject
{
    /// <summary>
    /// Mark Kayano's submission for the Procore technical take home challenge, which is to implement the Sieve of Eratosthenes. 
    /// There is a minor addition which uses the api provided by http://numbersapi.com/.
    /// Using this API, we generate a random fact about the selected number in our gridview.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void btnCalculatePrimeNums_Click(object sender, RoutedEventArgs e)
        {
            int upperBound = 0;
            if (isValidInput(txtUpperBound.Text, out upperBound))
            {
                List<int> primeValues = getPrimes(upperBound);
                gvPrimeNums.ItemsSource = primeValues;
            }
            else
            {
                await new MessageDialog("This program currently accepts whole numbers greater than or equal to 2.").ShowAsync();
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            gvPrimeNums.ItemsSource = null;
            txtUpperBound.Text = "";
        }

        // we are only going to be accepting input greater than or equal to 2
        private bool isValidInput(string input, out int upperBound)
        {
            upperBound = 0;
            return (input != "" && int.TryParse(txtUpperBound.Text, out upperBound) && upperBound >= 2);
        }

        private List<int> getPrimes(int upperBound)
        {
            int counter;
            int sqrtOfUpperBound = (int)Math.Sqrt(upperBound);
            bool[] isPrime = new bool[upperBound + 1];
            List<int> primeValues = new List<int>();

            // initialize all of our boolean values to true, starting at 2 because it is the first prime number
            for (int i = 2; i <= upperBound; i++)
            {
                isPrime[i] = true;
            }

            // add 2 to our prime value list, because we already know it's a prime number and we won't be checking it 
            primeValues.Add(2);

            // mark our boolean array for prime values
            for (counter = 3; counter <= sqrtOfUpperBound; counter += 2) // we start at 3 and increment by 2 because we do not need to check even values if they are prime or not
            {
                if (isPrime[counter])
                {
                    int increment = counter + counter;
                    primeValues.Add(counter);

                    // we will enumerate through the multiples of our prime number and mark them not prime, starting with the prime number squared
                    for (int j = counter * counter; j <= upperBound; j += increment)
                    {
                        isPrime[j] = false;
                    }

                }
            }

            // at this point, we've marked the numbers appropriately in our boolean array
            // now, we iterate through the rest of the odd numbers (after the square root of the input) in the array, adding prime numbers to our list
            // any odd prime numbers would've been caught by the inner for loop above
            for (; counter <= upperBound; counter += 2)
            {
                if (isPrime[counter])
                {
                    primeValues.Add(counter);
                }
            }
            return primeValues;
        }

        private async void gvPrimeNums_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                await RunAsync((int)e.ClickedItem);
            }
            catch (Exception)
            {
                await new MessageDialog("An error occured communicating with the API, please try again.").ShowAsync();
            }
        }
        private async Task RunAsync(int input)
        {
            NumAPI numberInfo = await NumAPI.GetRequest(input);
            await new MessageDialog(numberInfo.text).ShowAsync();
        }

    }
}
